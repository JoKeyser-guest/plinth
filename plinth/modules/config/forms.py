#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
"""
Forms for basic system configuration
"""

import logging
import re

from django import forms
from django.core import validators
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _
from django.utils.translation import ugettext_lazy

from plinth import cfg, frontpage
from plinth.utils import format_lazy

logger = logging.getLogger(__name__)

HOSTNAME_REGEX = r'^[a-zA-Z0-9]([-a-zA-Z0-9]{,61}[a-zA-Z0-9])?$'


def domain_label_validator(domainname):
    """Validate domain name labels."""
    for label in domainname.split('.'):
        if not re.match(HOSTNAME_REGEX, label):
            raise ValidationError(_('Invalid domain name'))


def get_homepage_choices():
    shortcuts = frontpage.get_shortcuts(web_apps_only=True, sort_by='name')
    apps = [(shortcut['id'], shortcut['name']) for shortcut in shortcuts
            if shortcut['id']]
    apache_default = ('apache-default', _('Apache Default'))
    plinth = ('plinth', _('FreedomBox Service (Plinth)'))
    return [apache_default, plinth] + apps


class ConfigurationForm(forms.Form):
    """Main system configuration form"""
    # See:
    # https://tools.ietf.org/html/rfc952
    # https://tools.ietf.org/html/rfc1035#section-2.3.1
    # https://tools.ietf.org/html/rfc1123#section-2
    # https://tools.ietf.org/html/rfc2181#section-11
    hostname = forms.CharField(
        label=ugettext_lazy('Hostname'), help_text=format_lazy(
            ugettext_lazy(
                'Hostname is the local name by which other devices on the local '
                'network can reach your {box_name}.  It must start and end with '
                'an alphabet or a digit and have as interior characters only '
                'alphabets, digits and hyphens.  Total length must be 63 '
                'characters or less.'), box_name=ugettext_lazy(cfg.box_name)),
        validators=[
            validators.RegexValidator(HOSTNAME_REGEX,
                                      ugettext_lazy('Invalid hostname'))
        ], strip=True)

    domainname = forms.CharField(
        label=ugettext_lazy('Domain Name'), help_text=format_lazy(
            ugettext_lazy(
                'Domain name is the global name by which other devices on the '
                'Internet can reach your {box_name}.  It must consist of labels '
                'separated by dots.  Each label must start and end with an '
                'alphabet or a digit and have as interior characters only '
                'alphabets, digits and hyphens.  Length of each label must be 63 '
                'characters or less.  Total length of domain name must be 253 '
                'characters or less.'), box_name=ugettext_lazy(cfg.box_name)),
        required=False, validators=[
            validators.RegexValidator(
                r'^[a-zA-Z0-9]([-a-zA-Z0-9.]{,251}[a-zA-Z0-9])?$',
                ugettext_lazy('Invalid domain name')), domain_label_validator
        ], strip=True)

    homepage = forms.ChoiceField(
        label=ugettext_lazy('Webserver Home Page'), help_text=format_lazy(
            ugettext_lazy(
                'Choose the default page that must be served when '
                'someone visits your {box_name} on the web. A typical use '
                'case is to set your blog or wiki as the home page when '
                'someone visits the domain name. Note that once the home '
                'page is set to something other than {box_name} Service '
                '(Plinth), your users must explicitly type /plinth or '
                '/freedombox to reach {box_name} Service (Plinth).'),
            box_name=ugettext_lazy(cfg.box_name)), required=False,
        choices=get_homepage_choices)
